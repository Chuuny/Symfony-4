<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
// Spécifié qu'on utilise l'entité Article
use App\Entity\Article;
class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // La boucle est executé 10 fois, donc 10 articles créé
        for($i = 1; $i <= 10; $i++){
            // new Article correspond au nom de ma fixture qui est une classe
            $article = new Article();
            // set+Nom des setteurs que doctrine a créé dans la classe Article
            $article->setTitle("Titre de l'article numéro $i")
                    ->setContent("Contenu de l'article")
                    ->setImage("http://placehold.it/350x150")
                    ->setCreatedAt(new \Datetime());
                $manager->persist($article);
        }

        $manager->flush();
    }
}
