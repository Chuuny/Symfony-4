<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
// On récupère l'entité Article
use App\Entity\Article;
class BlogController extends Controller
{

    /**
     * Créer le chemin du fichier (à la racine)
     * @Route("/", name="home")
     */
    public function home(){
        // Permet d'appeler un fichier twig pour afficher
        return $this->render('blog/home.html.twig');
    }

    /**
     * @Route("/blog", name="listOfArticle")
     */
    public function listOfArticle()
    {
        // On récupère un répertoire
        $repo = $this->getDoctrine()->getRepository(Article::class);
        // Trouve tous les articles
        $articles = $repo->findAll();
        return $this->render('blog/listOfArticle.html.twig',[
            // Affiche tous les articles
            'articles' => $articles
        ]);
    }
    /**
     * @Route("/blog/new", name="newArticle")
     */
    public function newArticle(){
        return $this->render('blog/newArticle.html.twig');
    }
    /**
     * Pourquoi cette fonction doit être appelé en dernier ? Sinon erreur
     * @Route("/blog/{id}", name="article")
     */
    public function show($id){
        $repo = $this->getDoctrine()->getRepository(Article::class);
        $article = $repo->find($id);
        return $this->render('blog/show.html.twig',[
            'article' => $article
        ]);
    }

}
